import cssModule from "./App.modules.css";
import image from "./assets/images/48.jpg";

function App() {
  return (
    <div className="App">
      <div className={cssModule.dcContainer}>
        <div className={cssModule.dcImageContainer}>
          <img src={image} alt='image user' className={cssModule.dcImage}></img>
        </div>
        <div className='dc-quote'>
          <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
        </div>
        <div className={cssModule.dcInfo}>
          <b>Tammy Stevens</b> &nbsp; * &nbsp; Font End Developer
        </div>
      </div>
    </div>
  );
}

export default App;